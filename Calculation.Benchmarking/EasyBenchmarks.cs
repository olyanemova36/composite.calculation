using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Library;

namespace Calculation.Benchmarking
{
    [MemoryDiagnoser]
    [SimpleJob(RunStrategy.Throughput, warmupCount: 5, targetCount: 5)]
    public class EasyBenchmarks
    {
        [Params(5, 10, 25, 50, 100, 150, 250, 300, 450, 500, 1000, 1500, 2000)] public int Dimension;
        Random rnd = new Random();
        private CalculationTree<int> mapOperation;
        private CalculationTree<int> reduceOperation;
        private CalculationTree<int> productOperation;
        private CalculationTree<int> zipOperation;
        [GlobalSetup]
        public void GlobalSetup()
        {
            var list = new ConcurrentBag<ConcurrentBag<int>>();
            var doubleList = new ConcurrentBag<ConcurrentBag<int>>();
            for (var i = 0; i < Dimension; i++)
            {
                list.Add(new ConcurrentBag<int>{rnd.Next()});
                doubleList.Add(new ConcurrentBag<int>{rnd.Next(), rnd.Next()});
            }
            
            var counts = new DataNode<int>(list);
            var douleCounts = new DataNode<int>(doubleList);
            var otherCounts = new DataNode<int>(new ConcurrentBag<ConcurrentBag<int>>{new ConcurrentBag<int>{2}});

            productOperation = Execution<int>.Compution(
                Execution<int>.productNode(PRODUCT.ONCOUNT, new List<Node<int>> {counts, otherCounts}));
            reduceOperation = Execution<int>.Compution(
                Execution<int>.reduceNode(REDUCE.SUM, counts));
            mapOperation = Execution<int>.Compution(
                Execution<int>.mapNode(MAP.MULTIPLY, douleCounts));
            zipOperation = Execution<int>.Compution(
                Execution<int>.zipNode(ZIP.TWOTOGHETHER, new List<Node<int>> {counts, otherCounts}));
        }
    
        [Benchmark]
        public void ThreadedMap() { mapOperation.Calculate(); }
        
        [Benchmark]
        public void ThreadedZip() { zipOperation.Calculate(); }
        
        [Benchmark]
        public void ThreadedProduct() { productOperation.Calculate(); }
        
        [Benchmark]
        public void ThreadedReduce() { reduceOperation.Calculate(); }
    }
}