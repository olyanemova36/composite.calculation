using System;
using System.Collections.Generic;
using Calculation.Protobuf.Tree;
using Google.Protobuf.Collections;
using Library;
using OPERATION = Calculation.Protobuf.Tree.OPERATION;

namespace Calculation.Worker
{
    public static class Algorithms
    {
        public static Result Calculate(CalculationTree ms)
        {
            var tree = Iniligate(ms);
            tree.Calculate();
            Console.WriteLine(tree.getResult());
            Result result = new Result()
            {
                Res = tree.getResult()
            };
            return result;
        }
        
        public static CalculationTree<double> Iniligate(CalculationTree ms)
        {
            /*
            var counts = new DataNode<int>(new List<List<int>>{new List<int>{3}, new List<int>{2}, new List<int>{5}, new List<int>{5}, new List<int>{5},  new List<int>{3}});
            var two = new DataNode<int>(new ConcurrentBag<ConcurrentBag<int>>{new ConcurrentBag<int>{2}});

            var tree = Execution<int>.Compution(Execution<int>.reduceNode(REDUCE.SUM,
                Execution<int>.mapNode(MAP.MULTIPLY,
                    Execution<int>.productNode(PRODUCT.ONCOUNT, new List<Node<int>> {counts, two}))));
             */
            CalculationTree<double> tree = new CalculationTree<double>(null);
            var temp = ms.Head;
            var prev = ms.Head;
            if (temp.IAm == WHO.Assignment)
            {
                var newNode = nodeIniligate(temp);
                if (tree.getRoot() == null)
                {
                    tree.pushNode(newNode);
                    Console.WriteLine("SUCCESS");
                }
            }
            return tree;
        }

        public static Node<double>? nodeIniligate(Node template)
        {
            if (template.Left != null)
            {
                switch (template.AgregateOp)
            {
                case OPERATION.Reduce:
                    if (template.Left.IAm == WHO.Assignment)
                    {
                        Console.WriteLine("Push reduce");
                        return Execution<double>.reduceNode(reduceOperations(template.TaskType), nodeIniligate(template.Left));
                    }
                    else if (template.Left.IAm == WHO.Data && template.Right.IAm == WHO.Data)
                    {
                        return Execution<double>.reduceNode(reduceOperations(template.TaskType),
                            new List<Node<double>> {dataInilidate(template.Left), dataInilidate(template.Right)}
                        );
                    }
                    else if (template.Left.IAm == WHO.Data)
                    {
                        return Execution<double>.reduceNode(reduceOperations(template.TaskType),
                            dataInilidate(template.Left)
                        );
                    }
                    else
                    {
                        return new DataNode<double>(new List<List<double>>{new List<double>{-1}});
                    }
                case OPERATION.Product:
                    if (template.Left.IAm == WHO.Assignment)
                    {
                        return Execution<double>.productNode(productOperations(template.TaskType), nodeIniligate(template.Left));
                    }
                    else if (template.Left.IAm == WHO.Data && template.Right.IAm == WHO.Data)
                    {
                        Console.WriteLine("Push product");
                        return Execution<double>.productNode(productOperations(template.TaskType),
                            new List<Node<double>> {dataInilidate(template.Left), dataInilidate(template.Right)}
                        );
                    }
                    else if (template.Left.IAm == WHO.Data)
                    {
                        return Execution<double>.productNode(productOperations(template.TaskType),
                            dataInilidate(template.Left)
                        );
                    }
                    else
                    {
                        return new DataNode<double>(new List<List<double>>{new List<double>{-1}});
                    }
                case OPERATION.Zip:
                    if (template.Left.IAm == WHO.Assignment)
                    {
                        return Execution<double>.zipNode(zipOperations(template.TaskType), nodeIniligate(template.Left));
                    }
                    else if (template.Left.IAm == WHO.Data && template.Right.IAm == WHO.Data)
                    {
                        return Execution<double>.zipNode(zipOperations(template.TaskType),
                            new List<Node<double>> {dataInilidate(template.Left), dataInilidate(template.Right)}
                        );
                    }
                    else if (template.Left.IAm == WHO.Data)
                    {
                        return Execution<double>.zipNode(zipOperations(template.TaskType),
                            dataInilidate(template.Left)
                        );
                    }
                    else
                    {
                        return new DataNode<double>(new List<List<double>>{new List<double>{-1}});
                    }
                case OPERATION.Map:
                    if (template.Left.IAm == WHO.Assignment)
                    {
                        Console.WriteLine("Push map");
                        return Execution<double>.mapNode(mapOperations(template.TaskType), nodeIniligate(template.Left));
                    }
                    else if (template.Left.IAm == WHO.Data && template.Right.IAm == WHO.Data)
                    {
                        return Execution<double>.mapNode(mapOperations(template.TaskType),
                            new List<Node<double>> {dataInilidate(template.Left), dataInilidate(template.Right)}
                        );
                    }
                    else if (template.Left.IAm == WHO.Data)
                    {
                        return Execution<double>.mapNode(mapOperations(template.TaskType),
                            dataInilidate(template.Left)
                        );
                    }
                    else
                    {
                        return new DataNode<double>(new List<List<double>>{new List<double>{-1}});
                    }
                default:
                    return new DataNode<double>(new List<List<double>>{new List<double>{-1}});
            }
            }
            return new DataNode<double>(new List<List<double>>{new List<double>{-1}});
        }
        public static DataNode<double>? dataInilidate(Node data)
        {
            var info = new List<List<double>>();
            foreach (var miniList in data.Listy)
            {
                info.Add(transformationList(miniList.Doubles));
            }
            return new DataNode<double>(info);
        }
        public static List<List<double>> transformRepeated(Node data)
        {
            var info = new List<List<double>>();
            foreach (var miniList in data.Listy)
            {
                info.Add(transformationList(miniList.Doubles));
            }
            return info;
        }
        public static List<double> transformationList(RepeatedField<double> data)
        {
            var info = new List<double>();
            foreach (var item in data)
            {
                info.Add(item);
            }
            return info;
        }

        public static MAP mapOperations(TASK type)
        {
            if (type == TASK.Multiply) { return MAP.MULTIPLY; }
            if (type == TASK.Power) { return MAP.POWER; } 
            return MAP.DERIVE;
        }
        public static ZIP zipOperations(TASK type)
        {
            if (type == TASK.Twotoghether) { return ZIP.TWOTOGHETHER; }
            return ZIP.FROMONE;
        }
        public static PRODUCT productOperations(TASK type)
        {
            if (type == TASK.Oncount) { return PRODUCT.ONCOUNT; }
            return PRODUCT.ONCOUNT;
        }
        public static REDUCE reduceOperations(TASK type)
        {
            if (type == TASK.Count) { return REDUCE.COUNT; }
            if (type == TASK.Sum) { return REDUCE.SUM; }
            if (type == TASK.Max) { return REDUCE.MAX; }
            return REDUCE.MIN; 
        }
    }
}