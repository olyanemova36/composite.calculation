﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using Calculation.Protobuf.Tree;
using Library;
using Microsoft.Extensions.Logging.Abstractions;
using OPERATION = Calculation.Protobuf.Tree.OPERATION;

namespace Calculation.Client
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            using var channel = GrpcChannel.ForAddress(
                "http://localhost:18081",
                new GrpcChannelOptions()
                {
                    Credentials = ChannelCredentials.Insecure,
                    LoggerFactory = new NullLoggerFactory()
                }
            );
            var mss = new CompositeComputionService.CompositeComputionServiceClient(channel);
            await Simple(mss);
        }

        private static async Task Simple(CompositeComputionService.CompositeComputionServiceClient mss)
        {
            CalculationTree tree = new CalculationTree()
            {
                Head = new Node()
                {
                    Left = new Node()
                    {
                        
                        Left =  new Node()
                        {
                            Left = new Node()
                            {
                                Listy =
                                {
                                    {new List0Double() {Doubles = {3}}}, {new List0Double() {Doubles = {2}}},
                                    {new List0Double() {Doubles = {5}}}, {new List0Double() {Doubles = {5}}},
                                    {new List0Double() {Doubles = {5}}}, {new List0Double() {Doubles = {3}}}
                                },
                                IAm = WHO.Data
                            }, 
                            Right = new Node()
                            {
                                Listy =
                                {
                                    {new List0Double() {Doubles = {2}}}
                                },
                                IAm = WHO.Data
                            },
                            TaskType = TASK.Oncount,
                            AgregateOp = OPERATION.Product,
                            IAm = WHO.Assignment
                        },
                        Right = null,
                        TaskType = TASK.Multiply,
                        AgregateOp = OPERATION.Map,
                        IAm = WHO.Assignment,
                    },
                    Right = null,
                    TaskType = TASK.Sum,
                    AgregateOp = OPERATION.Reduce,
                    IAm = WHO.Assignment,
                }
            };
            //CalculationTree tree = Mapper.Map(calculation);
            var result = await mss.CalculationAsync(tree);
            Console.WriteLine(result.ToString());
        }
        
    }
}