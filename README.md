# API for compositional description of calculations

__Objective__: to develop an algorithm for compositional calculations accelerated with the help of parallel programming skills.

__Base goal__: the parallelism of calculations is achieved due to the multithreaded implementation of individual operations (such as map, flatMap and the special case of reduce/fold, when the argument function is an associative operation).

## Algorithm's description

Calculations were performed using a compiled expression tree consisting of `Tasknode's` and `Datanode's`. The tree sheets could only contain data sheets (`Datanode's`). In the nodes, respectively, nodes with tasks from the operations listed above.


- __Algorithm for the map operation__

  At the input, we get a sheet of pairs of values and the type of operation. For example, addition. It is required to get the result of the expression (x, y) -> (z) : z = x + y.
  The algorithm is simple: use a foreach loop to go through all the elements of the sheet and add its first and second components. The result is also returned as `List<List<Double>>`.

- __Algorithm for the reduce operation__

  At the input, we get a sheet of values and the type of operation. For example, the maximum. It is required to get the maximum.
  The algorithm is simple: use a foreach loop to go through all the elements of the sheet and select the maximum. If there are several values inside one list item, then it is required to return a list of the maxima in each list of the list of values. The result is also returned as `List<List<Double>>`.
- __Algorithm for zip operation__

  We get two sheets of values as input. It is required to get the result of concatenation of two sheets on the inner sheets. The correspondence is made on a sheet of minimum length.
  The algorithm is simple: use a foreach loop to simultaneously go through all the elements of two sheets and put the corresponding i-th elements of the sheets in the resulting sheet. The result is also returned as `List<List<Double>>`.
- __Algorithm for the product operation__

  We get two sheets of values as input. It is required to obtain the result of multiplying two sheets by inner sheets.

Example:

```mermaid
graph TD;
    Expr.Reduce.Max-->Expr.Map.multiply;
    Expr.Map.multiply-->Expr.Product;
    Expr.Product-->ArrayList_Counts;
    Expr.Product-->ArrayList_OneCount;
```
_Scheme 1. Graph of Compositional Compution._

It displays equation like:
```math
    max((2*2),(3*2),(4*2))
```

__Algorithm of parallelization of calculations:__
1. Creating a basic ThreadPool that collects a queue of tasks for execution
2. Inclusion of this ThreadPool in the cycles of passing through the sheets and collecting calculation packages:

   A) Implementation 1 assumed the collection of calculations by the piece. That is, all calculations were first placed in a queue, and then executed.

   B) Implementation 2 assumed the collection of calculations in a package of 100 pieces. I.e., packages of 100 calculations in each were executed in parallel.

3. The ThreadPool operation was framed by a CountDown counter in both implementations in order to perform tasks only when the required number of them is typed
   , the "Control tree of calculations" has the form:
```c#
    tree = Computation.reduce(Reduce.sum,
        Computation.map(Map.multiply,
            Computation.product(Product.product,list2,
                Computation.map(Map.multiply,
                    Computation.product(Product.product,
                         list1, list2)))));
```

## Statistic of parallelization of calculations

The code was re-analyzed with the implementation of the algorithm in parallel and non-parallel modes.

![](images/Statistics_CC.png)


_Picture 1. Description of parallel statistics._

Parallelization of calculations in the nodes of the tree gave poor results due to the fact that waiting for tasks in the Threadpool took a very long time than completing the task itself. With the Reduce operation, the results became even worse, since here I tried to implement a different method of parallelization, which is shown in detail in the code./Library/Execute.cs.

## Description of the distributed system architecture

The developed system consists of the following components:
* Client application - sending a request to perform calculations via console input (not yet implemented).
* Manager - a service that accepts requests from the client, connects the data coming with the request with Apache Kafka for further processing.
* Workers – the part of the system responsible for performing the calculations necessary to send a response to a request. Workers subscribe to patricia to perform tasks transferred to Apache Kafka.
* Kafka is a system that allows you to scale the developed application horizontally, that is, to create an arbitrary number of tasks for Workers to calculate.

![](images/Components_diagram-2.svg)

_Scheme 2. Diagram of the distributed system architecture._

## User documentation

For the correct launch, an important step is to check the network in which all system components will be connected, a custom wi-fi network of developers was used for this project. To change the ip inside which you are going to raise the system and place components there, change this number in all docker-compose files.yaml, in order to avoid errors during compilation of the project.
To raise the system, and it worked correctly:
1. Raise Kafka by assembling docker-compose.yaml, which is located in the kafka-mss folder.
2. Create topics for Workers and Manager in Apache Kafka to store tasks with requests and tasks with responses to these requests there.
   Do it with a script init.sh in the same folder.
3. Build the Calculation.Manager and Calculation images.Worker using the build-images script.sh in the root folder of the project.
4. Run the Calculation.Manager and Calculation images.Worker using docker-compose.yaml in the appropriate folders to run their images.
   You should adjust the number of Workers to perform computational tasks through the script start.sh in the Calculation project folder.Worker. It is worth remembering that the number of Workers is related to the partitions in Apache Kafka, you regulate them with reason.
5. Launch the client application implemented in the Calculation.Client project. Currently, you can do this by running a regular console application through the Rider interface or the development environment you use.
   The application will send a request and after a while will return a response to your calculation in the form:

                                     { res : "number” }
6. After the system usage needs expire, shut down by extinguishing all running images via docker-compose and delete the folder with Apache Kafka logs before restarting. This is the kafka-data folder in the underlying root path from the solution./kafka-mss/kafka-data.

Important! Before launching or changing the number of Workers, change the name of the group of Workers so that they do not get confused and idle between each other.

To use the application correctly at the moment, you need to send a request to the command line of your Terminal.

```shell
    cd kafka-mss 
    docker-compose up -d
     ./init.sh 
    cd.. 
    docker build -t computecalculation/worker:1.0.0 -f Calculation.Worker/Dockerfile . 
    docker build -t computecalculation/manager:1.0.0 -f Calculation.Manager/Dockerfile . 
    cd Calculation.Worker 
    ./start.sh 
    cd.. 
    cd Calculation.Manager 
    docker-compose up -d 
    сd .. 
    cd Calculation.Client #Load like console app

```
