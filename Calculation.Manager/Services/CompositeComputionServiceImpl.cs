using System;
using System.Threading.Tasks;
using Grpc.Core;
using Calculation.Manager.Kafka;
using Calculation.Worker.Protobuf;
using Calculation.Protobuf.Tree;
using Serilog;

namespace Calculation.Manager.Services
{
    public class CompositeComputionServiceImpl : CompositeComputionService.CompositeComputionServiceBase
    {
        public override async Task<Result> Calculation(CalculationTree request, ServerCallContext context)
        {
            Log.Information("Handling new request");
            var correlationId = Guid.NewGuid();
            Log.Information("Generated corrId: {CorrelationId}", correlationId);
            await KafkaAdapter.ProduceAsync(
                correlationId,
                new WorkerRq
                {
                    CorrelationId = correlationId.ToString(),
                    RqData = request
                }
            );
            Log.Information("Awaiting for result");
            await foreach (var result in KafkaAdapter.Consume(correlationId))
            {
                Log.Information("Receiving result");
                return result.RsData;
            }
            throw new Exception("Unreachable area");
        }
    }
}